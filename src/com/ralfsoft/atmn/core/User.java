/**
 * 
 */
package com.ralfsoft.atmn.core;

/**
 * @author Melwyn
 *
 */
public class User {

	String name;
	String password;
	
	public User(String name, String password){
		this.name = name;
		this.password = password;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	
}
