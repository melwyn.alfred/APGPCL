/**
 * 
 */
package com.ralfsoft.atmn.platform.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.ralfsoft.atmn.driver.UISetup;

/**
 * @author Melwyn
 *
 */
public class Home extends UISetup {

	WebDriver driver;
	
	public Home(WebDriver webdriver){
		setDriver(webdriver);
	}
	
	public void navigateToSignIn(){
		driver.get(getHomeURL());
	}
	
	public boolean verifyHomePageText(String text){
		return pageContent().getText().equals(text);
	}

	public WebElement welcomeText(){
		return getDriver().findElement(By.cssSelector("td.tdContentHeader"));
	}
	
	public WebElement aboutUs(){
		return getDriver().findElement(By.cssSelector("a[href='Aboutus.aspx']"));
	}
	
	public WebElement aboutUsContent(){
		return getDriver().findElement(By.cssSelector(".tdContent > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(1)"));
	}
	
	public WebElement pageContent(){
		return getDriver().findElement(By.cssSelector(".tdContentLeft > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(1)"));
	}
    
	public boolean isWelcomeTextDisplayed(){
		return welcomeText().isDisplayed();
	}
	
	public boolean verifyAboutUsContent(String text){
		return aboutUsContent().getText().equals(text);
	}

	public WebDriver getDriver() {
		return driver;
	}


	public void setDriver(WebDriver driver) {
		this.driver = driver;
	} 
	
	
}
