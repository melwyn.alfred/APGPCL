/**
 * 
 */
package com.ralfsoft.atmn.core;

import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Date;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

/**
 * @author Melwyn
 *
 */
public class Utils {
	
	
	 public static void captureScreenShot(WebDriver ldriver){

		  // Take screenshot and store as a file format
		  File src = ((TakesScreenshot)ldriver).getScreenshotAs(OutputType.FILE);
		try {
		  // now copy the  screenshot to desired location using copyFile method
			File screen = new File("./test-screenshots/"+System.currentTimeMillis()+".png");
			System.out.println(screen.getCanonicalPath());
		 FileUtils.copyFile(src, screen);
		       }

		catch (IOException e)

		{

		System.out.println(e.getMessage());

		    }

		}
	 
	 public static void captureScreenShot(String fileName) {
		 try{
	 BufferedImage image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
	 new File("./test-screenshots/").mkdirs();
	 File screen = new File("./test-screenshots/"+fileName+".png");
	 ImageIO.write(image, "png", new File(screen.getCanonicalPath()));
//	 new Utils().reportLogScreenshot(screen);
		 }
		 catch(Exception e){
			 System.out.println(e.getMessage());
		 }
	 }
	 
	 protected void reportLogScreenshot(File file) throws IOException {
	      System.setProperty("org.uncommons.reportng.escape-output", "false");
	  
	      String absolute = file.getAbsolutePath();
	      int beginIndex = absolute.indexOf(".");
	      String relative = absolute.substring(beginIndex).replace(".\\","");
//	      System.out.println(file.getCanonicalPath());
	      String screenShot = relative.replace('\\','/');  //file.getCanonicalPath().replaceAll("\\\\", "/"); //
	      System.out.println(screenShot);
	      
	Reporter.log("<a href=\"../../" + screenShot + "\"><p align=\"left\">Screenshot at " + new Date()+ "</p>");
	Reporter.log("<p><img width=\"1024\" src=\"../.." + screenShot  + "\" alt=\"screenshot at " + new Date()+ "\"/></p></a><br />"); 
	}
}
