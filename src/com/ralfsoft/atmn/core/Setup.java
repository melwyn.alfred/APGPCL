/**
 * 
 */
package com.ralfsoft.atmn.core;

import java.io.IOException;
import java.util.Properties;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;

/**
 * @author Melwyn
 *
 */
public class Setup {
	
	protected Properties urlProperties;
	protected Properties userProperties;
	protected static String homeURL;
	protected static String adminURL;
	protected String adminUsername;
	protected String adminPassword;
	
	@BeforeTest(alwaysRun=true)
	public void beforeTest() throws IOException{
		setUp();
	}
	
	public void setUp(){
		setUrlProperties(new PropertiesUtils().getProperties("./config/urls.properties"));
		setUserProperties(new PropertiesUtils().getProperties("./config/users.properties"));
		setHomeURL(getUrlProperties().getProperty("apgpcl.home"));
		setAdminURL(getUrlProperties().getProperty("apgpcl.admin"));	
		setAdminUsername(getUserProperties().getProperty("admin.username"));
		setAdminPassword(getUserProperties().getProperty("admin.password"));
		System.setProperty("org.uncommons.reportng.title", "APGPCL Test Results");
	}

	/**
	 * @return the adminUsername
	 */
	public String getAdminUsername() {
		return adminUsername;
	}

	/**
	 * @param adminUsername the adminUsername to set
	 */
	public void setAdminUsername(String adminUsername) {
		this.adminUsername = adminUsername;
	}

	/**
	 * @return the adminPassword
	 */
	public String getAdminPassword() {
		return adminPassword;
	}

	/**
	 * @param adminPassword the adminPassword to set
	 */
	public void setAdminPassword(String adminPassword) {
		this.adminPassword = adminPassword;
	}

	@BeforeClass()
	public void beforeClass(){
		
	}
	
	@AfterClass
	public void afterClass(){
		
	}
	
	@AfterTest
	public void afterTest(){
		
	}

	public Properties getUrlProperties() {
		return urlProperties;
	}

	public void setUrlProperties(Properties urlProperties) {
		this.urlProperties = urlProperties;
	}

	/**
	 * @return the homeURL
	 */
	public String getHomeURL() {
		return homeURL;
	}

	/**
	 * @param homeURL the homeURL to set
	 */
	public void setHomeURL(String homeURL) {
		Setup.homeURL = homeURL;
	}

	/**
	 * @return the adminURL
	 */
	public String getAdminURL() {
		return adminURL;
	}

	/**
	 * @param adminURL the adminURL to set
	 */
	public void setAdminURL(String adminURL) {
		Setup.adminURL = adminURL;
	}

	/**
	 * @return the userProperties
	 */
	public Properties getUserProperties() {
		return userProperties;
	}

	/**
	 * @param userProperties the userProperties to set
	 */
	public void setUserProperties(Properties userProperties) {
		this.userProperties = userProperties;
	}
}
