package com.ralfsoft.atmn.tests;

import java.lang.reflect.Method;

import org.openqa.selenium.Alert;
import org.openqa.selenium.UnhandledAlertException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.ralfsoft.atmn.core.Utils;
import com.ralfsoft.atmn.driver.UISetup;
import com.ralfsoft.atmn.platform.pageobjects.Admin;
import com.ralfsoft.atmn.platform.pageobjects.ControlPanelHome;
import com.ralfsoft.atmn.platform.pageobjects.ForgotPassword;
import com.ralfsoft.atmn.platform.pageobjects.Home;
import com.ralfsoft.atmn.platform.pageobjects.PageContent;


public class Tests extends UISetup{

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	@Test(groups ={Priority.FIVE}) 
	public void Login_Valid_Credentials(Method m) throws InterruptedException{
		Admin admin = new Admin(driver);
		admin.navigateToSignIn();
		waitForLoad();
		admin.login(getAdminUsername(), getAdminPassword());
		Thread.sleep(5*1000);
		assert admin.isWelcomeTextDisplayed() : "Welcome page not displayed... Application not launched... TC #1 failed.";
		Reporter.log("PASSED : TC #1: Login Access - APGPCL Control Panel - Valid Credentials");
		Utils.captureScreenShot(m.getName() + " TC#1_Passed");
		quitDriver();
	}
	
	@Test(groups ={Priority.FIVE})
	public void Login_Invalid_Credentials(Method m) throws InterruptedException{
		Admin admin = new Admin(driver);
		admin.navigateToSignIn();
		waitForLoad();
		admin.login("ABCDEF", "123456");
		Thread.sleep(5000);
		try{
		driver.get(getAdminURL());
		}
		catch (UnhandledAlertException e){
			Alert alert = driver.switchTo().alert();
	        String alertText = alert.getText();
	        Reporter.log("Alert data: " + alertText);
	        Utils.captureScreenShot(m.getName() + " TC#2_Passed");
			assert e.getMessage().contains("Invalid UserName or Password") : "Invalid credentials pop-up not displayed. TC #2 failed.";
			alert.accept();
		}
		Reporter.log("PASSED : TC #2: Login Access - APGPCL Control Panel - Invalid Credentials");
		quitDriver();
	}
	
	@Test(groups ={Priority.TWO})
	public void Return_To_Homepage(Method m){
		Admin admin = new Admin(driver);
		admin.navigateToSignIn();
		waitForLoad();
		admin.returnToHomepage().click();
		waitForLoad();
		Home home = new Home(driver);
		assert home.isWelcomeTextDisplayed() : "Home page not displayed... TC #3 failed.";
		Utils.captureScreenShot(m.getName() + " TC#3_Passed");
		Reporter.log("PASSED : TC #3: Accessing APGPCL Webpage - Control Panel Application");
		quitDriver();
	}
	
	@Test(groups ={Priority.THREE})
	public void Forgot_Password(Method m){
		Admin admin = new Admin(driver);
		admin.navigateToSignIn();
		waitForLoad();
		admin.forgotPassword().click();
		waitForLoad();
		ForgotPassword resetPass = new ForgotPassword(driver);
		assert resetPass.isPasswordResetDisplayed() : "Password Rest page not displayed... TC #4 failed.";
		Utils.captureScreenShot(m.getName() + " TC#4_Passed");
		Reporter.log("PASSED : TC #4: Password Reset Page - APGPCL Control Panel");
		quitDriver();
	}
	
	@Test(groups ={Priority.FOUR})
	public void Reset_Password_Valid(Method m) throws InterruptedException{
		Admin admin = new Admin(driver);
		admin.navigateToSignIn();
		waitForLoad();
		admin.forgotPassword().click();
		waitForLoad();
		ForgotPassword resetPass = new ForgotPassword(driver);
		resetPass.resetPassword(getAdminUsername(), "shalni41@gmail.com");
		Thread.sleep(5000);
		try{
		driver.get(getAdminURL());
		}
		catch (UnhandledAlertException e){
			Alert alert = driver.switchTo().alert();
	        String alertText = alert.getText();
	        Reporter.log("Alert data: " + alertText);
			Utils.captureScreenShot(m.getName() + " TC#5_Passed");
			assert e.getMessage().contains("Password sent to ur Email ID") : "Password sent pop-up not displayed. TC #5 failed.";
			alert.accept();
		}
		Reporter.log("PASSED : TC #5: Password Reset Page- APGPCL Control Panel - With Valid email");
		quitDriver();
	}
	
	@Test(groups ={Priority.THREE})
	public void Reset_Password_Invalid(Method m) throws InterruptedException{
		Admin admin = new Admin(driver);
		admin.navigateToSignIn();
		waitForLoad();
		admin.forgotPassword().click();
		waitForLoad();
		ForgotPassword resetPass = new ForgotPassword(driver);
		resetPass.resetPassword("BADDATA", "BADEMAIL@gmail.com");
		Thread.sleep(5000);
		try{
		driver.get(getAdminURL());
		}
		catch (UnhandledAlertException e){
			Alert alert = driver.switchTo().alert();
	        String alertText = alert.getText();
	        Reporter.log("Alert data: " + alertText);
			Utils.captureScreenShot(m.getName() + " TC#6_Passed");
			assert e.getMessage().contains("Invalid Email") : "Invalid Email pop-up not displayed. TC #6 failed.";
			alert.accept();
		}
		Reporter.log("PASSED : TC #6: Password Reset - APGPCL Control Panel - With Invalid email");
		quitDriver();
	}
	
	@Test(groups ={Priority.FOUR})
	public void Reset_Password_Cancel(Method m) throws InterruptedException{
		Admin admin = new Admin(driver);
		admin.navigateToSignIn();
		waitForLoad();
		admin.forgotPassword().click();
		waitForLoad();
		ForgotPassword resetPass = new ForgotPassword(driver);
		resetPass.enterDetails(getAdminUsername(), "shalni41@gmail.com");
		resetPass.btnCancel().click();
		Thread.sleep(5*1000);
		assert (admin.forgotPassword().isDisplayed()) : "Login page not displayed. TC #7 failed.";
		Utils.captureScreenShot(m.getName() + " TC#7_Passed");
		Reporter.log("PASSED : TC #7: Password Reset - APGPCL Control Panel - Cancel");
		quitDriver();
	}
	
	@Test(groups ={Priority.THREE}) 
	public void Home_Page(Method m) throws InterruptedException{
		Admin admin = new Admin(driver);
		admin.navigateToSignIn();
		waitForLoad();
		admin.login(getAdminUsername(), getAdminPassword());
		Thread.sleep(5*1000);
		assert admin.isWelcomeTextDisplayed() : "Home not displayed... Application not launched... TC #8 failed.";
		Reporter.log("PASSED : TC #8: Home Page - Control");
		Utils.captureScreenShot(m.getName() + " TC#8_Passed");
		quitDriver();
	}
	
	@Test(groups ={Priority.FOUR}) 
	public void Menu_Section(Method m) throws InterruptedException{
		Admin admin = new Admin(driver);
		admin.navigateToSignIn();
		waitForLoad();
		admin.login(getAdminUsername(), getAdminPassword());
		Thread.sleep(5000);
		ControlPanelHome cph = new ControlPanelHome(driver);
		assert cph.isMainMenuDisplayed() : "Main menu section not displayed... TC #9 failed.";
		Reporter.log("PASSED : TC #9: Menu Section - Control Panel Page");
		Utils.captureScreenShot(m.getName() + " TC#9_Passed");
		quitDriver();
	}
	
	@Test(groups ={Priority.FOUR}) 
	public void General_Sub_Menus(Method m) throws InterruptedException{
		Admin admin = new Admin(driver);
		admin.navigateToSignIn();
		waitForLoad();
		admin.login(getAdminUsername(), getAdminPassword());
		Thread.sleep(5000);
		ControlPanelHome cph = new ControlPanelHome(driver);
		cph.tabGeneral().click();
		assert cph.isGeneralSubMenuDisplayed() : "General sub menus not displayed... TC #10 failed.";
		Reporter.log("PASSED : TC #10: Sub Menus - General Menu Section");
		Utils.captureScreenShot(m.getName() + " TC#10_Passed");
		quitDriver();
	}
	
	@Test(groups ={Priority.TWO}) 
	public void Page_Content_Fields_Displayed(Method m) throws InterruptedException{
		Admin admin = new Admin(driver);
		admin.navigateToSignIn();
		waitForLoad();
		admin.login(getAdminUsername(), getAdminPassword());
		Thread.sleep(5000);
		ControlPanelHome cph = new ControlPanelHome(driver);
		cph.tabGeneral().click();
		cph.pageContents().click();
		Thread.sleep(2000);
		PageContent pc = new PageContent(driver);
		assert pc.isPageContentFieldsDisplayed() : "Page Content fields not displayed... TC #11 failed.";
		Reporter.log("PASSED : TC #11: General Menu- Page Content");
		Utils.captureScreenShot(m.getName() + " TC#11_Passed");
		quitDriver();
	}
	
	@Test(groups ={Priority.THREE}) 
	public void Verify_Select_Page_List(Method m) throws InterruptedException{
		Admin admin = new Admin(driver);
		admin.navigateToSignIn();
		waitForLoad();
		admin.login(getAdminUsername(), getAdminPassword());
		Thread.sleep(5000);
		ControlPanelHome cph = new ControlPanelHome(driver);
		cph.tabGeneral().click();
		cph.pageContents().click();
		Thread.sleep(2000);
		PageContent pc = new PageContent(driver);
		assert pc.verifySelectPageList() : "Expected select page list not present... TC #12 failed.";
		Reporter.log("PASSED : TC #12: Select Page List - Page Content Page");
		Utils.captureScreenShot(m.getName() + " TC#12_Passed");
		quitDriver();
	}
	
	@Test(groups ={Priority.TWO}) 
	public void Verify_Page_Formatting(Method m) throws InterruptedException{
		Admin admin = new Admin(driver);
		admin.navigateToSignIn();
		waitForLoad();
		admin.login(getAdminUsername(), getAdminPassword());
		Thread.sleep(5000);
		ControlPanelHome cph = new ControlPanelHome(driver);
		cph.tabGeneral().click();
		cph.pageContents().click();
		Thread.sleep(2000);
		PageContent pc = new PageContent(driver);
		assert pc.verifyPageContentFormatting() : "Page not formatted... TC #13 failed.";
		Reporter.log("PASSED : TC #13: Description - Formatting Options - Page Content Page");
		Utils.captureScreenShot(m.getName() + " TC#13_Passed");
		quitDriver();
	}
	
	@Test(groups ={Priority.THREE})
	public void Validate_Home_Page(Method m) throws InterruptedException{
		Admin admin = new Admin(driver);
		admin.navigateToSignIn();
		waitForLoad();
		admin.returnToHomepage().click();
		waitForLoad();
		Home home = new Home(driver);
		String homePageText = home.pageContent().getText();
		admin.navigateToSignIn();
		waitForLoad();
		admin.login(getAdminUsername(), getAdminPassword());
		Thread.sleep(5000);
		ControlPanelHome cph = new ControlPanelHome(driver);
		cph.tabGeneral().click();
		cph.pageContents().click();
		Thread.sleep(2000);
		PageContent pc = new PageContent(driver);
		pc.selectPage().selectByVisibleText("Home");
		assert pc.verifyPageContentText(homePageText) : "Home page text not displayed... TC #14 failed.";
		Reporter.log("PASSED : TC #14: Select Home Page Description - Page Content Page");
		Utils.captureScreenShot(m.getName() + " TC#14_Passed");
		quitDriver();
	}
	
	@Test(groups ={Priority.FIVE})
	public void Update_Home_Page_Description(Method m) throws InterruptedException{
		Admin admin = new Admin(driver);
		admin.navigateToSignIn();
		waitForLoad();
		admin.login(getAdminUsername(), getAdminPassword());
		Thread.sleep(5000);
		ControlPanelHome cph = new ControlPanelHome(driver);
		cph.tabGeneral().click();
		cph.pageContents().click();
		Thread.sleep(2000);
		PageContent pc = new PageContent(driver);
		pc.changePageContentText("sample123");
		pc.btnSave().click();
		Reporter.log("PASSED : TC #15: Update Home Page Description - Page Content Page");
		Utils.captureScreenShot(m.getName() + " TC#15_Passed");
		waitForLoad();
		quitDriver();
	}
	
	@Test(dependsOnMethods={"Update_Home_Page_Description"}, groups ={Priority.FIVE})
	public void Update_Home_Page_Description_Continued(Method m){
		Admin admin = new Admin(driver);
		admin.navigateToSignIn();
		waitForLoad();
		admin.returnToHomepage().click();
		waitForLoad();
		Home home = new Home(driver);
		assert home.verifyHomePageText("sample123") : "Updated Home page text not displayed... TC #16 failed.";
		Reporter.log("PASSED : TC #16: Home Page Description - Actual APGPCL site");
		Utils.captureScreenShot(m.getName() + " TC#16_Passed");
		quitDriver();
	}
	
	@Test(groups ={Priority.TWO})
	public void Update_Home_Page_Description_No_Text(Method m) throws InterruptedException{
		Admin admin = new Admin(driver);
		admin.navigateToSignIn();
		waitForLoad();
		admin.login(getAdminUsername(), getAdminPassword());
		Thread.sleep(5000);
		ControlPanelHome cph = new ControlPanelHome(driver);
		cph.tabGeneral().click();
		cph.pageContents().click();
		Thread.sleep(2000);
		PageContent pc = new PageContent(driver);
		pc.changePageContentText("");
		pc.btnSave().click();
		Reporter.log("PASSED : TC #17: Update Home Page Description (no text)- Page Content Page");
		Utils.captureScreenShot(m.getName() + " TC#17_Passed");
		quitDriver();
	}
	
	@Test(dependsOnMethods={"Update_Home_Page_Description_No_Text"}, groups = {Priority.TWO})
	public void Verify_Updated_Home_Page_No_Text(Method m){
		Admin admin = new Admin(driver);
		admin.navigateToSignIn();
		waitForLoad();
		admin.returnToHomepage().click();
		waitForLoad();
		Home home = new Home(driver);
		assert home.verifyHomePageText("") : "Updated Home page text not displayed... TC #18 failed.";
		Reporter.log("PASSED : TC #18: Home Page Description (with no text) - Actual APGPCL site");
		Utils.captureScreenShot(m.getName() + " TC#18_Passed");
		quitDriver();
	}
	
	@Test(groups ={Priority.THREE})
	public void Validate_About_Us(Method m) throws InterruptedException{
		Admin admin = new Admin(driver);
		admin.navigateToSignIn();
		waitForLoad();
		admin.returnToHomepage().click();
		waitForLoad();
		Home home = new Home(driver);
		home.aboutUs().click();
		waitForLoad();
		String aboutUsText = home.aboutUsContent().getText();
		admin.navigateToSignIn();
		waitForLoad();
		admin.login(getAdminUsername(), getAdminPassword());
		Thread.sleep(5000);
		ControlPanelHome cph = new ControlPanelHome(driver);
		cph.tabGeneral().click();
		cph.pageContents().click();
		Thread.sleep(2000);
		PageContent pc = new PageContent(driver);
		pc.selectPage().selectByVisibleText("About Us");
		Thread.sleep(4000);
		assert pc.verifyPageContentText(aboutUsText) : "About Us content not displayed... TC #19 failed.";
		Reporter.log("PASSED : TC #19: Select 'About Us' option in Select Page ");
		Utils.captureScreenShot(m.getName() + " TC#19_Passed");
		quitDriver();
	}
		
	@Test(groups ={Priority.ONE})
	public void Update_About_Us_Description(Method m) throws InterruptedException{
		Admin admin = new Admin(driver);
		admin.navigateToSignIn();
		waitForLoad();
		admin.login(getAdminUsername(), getAdminPassword());
		Thread.sleep(5000);
		ControlPanelHome cph = new ControlPanelHome(driver);
		cph.tabGeneral().click();
		cph.pageContents().click();
		Thread.sleep(2000);
		PageContent pc = new PageContent(driver);
		pc.selectPage().selectByVisibleText("About Us");
		Thread.sleep(4000);
		pc.changePageContentText("sample123");
		pc.btnSave().click();
		Reporter.log("PASSED : TC #20: Update 'About Us' content in Page content page");
		Utils.captureScreenShot(m.getName() + " TC#20_Passed");
		admin.isWelcomeTextDisplayed();
		quitDriver();
	}
	
	@Test(dependsOnMethods={"Update_About_Us_Description"}, groups ={Priority.ONE})
	public void Update_About_Us_Description_Continued(Method m){
		Admin admin = new Admin(driver);
		admin.navigateToSignIn();
		waitForLoad();
		admin.returnToHomepage().click();
		waitForLoad();
		Home home = new Home(driver);
		home.aboutUs().click();
		assert home.verifyAboutUsContent("sample123") : "Updated About Us text not displayed... TC #21 failed.";
		Reporter.log("PASSED : TC #21: About Us' Description - Actual APGPCL site");
		Utils.captureScreenShot(m.getName() + " TC#21_Passed");
		quitDriver();
	}
	
	@Test(groups ={Priority.ONE})
	public void Update_About_Us_Description_No_Text(Method m) throws InterruptedException{
		Admin admin = new Admin(driver);
		admin.navigateToSignIn();
		waitForLoad();
		admin.login(getAdminUsername(), getAdminPassword());
		Thread.sleep(5000);
		ControlPanelHome cph = new ControlPanelHome(driver);
		cph.tabGeneral().click();
		cph.pageContents().click();
		Thread.sleep(2000);
		PageContent pc = new PageContent(driver);
		pc.selectPage().selectByVisibleText("About Us");
		Thread.sleep(4000);
		pc.changePageContentText("");
		pc.btnSave().click();
		Reporter.log("PASSED : TC #22: Delete the 'About Us' content in Page content page");
		Utils.captureScreenShot(m.getName() + " TC#22_Passed");
		admin.isWelcomeTextDisplayed();
		quitDriver();
	}
	
	@Test(dependsOnMethods={"Update_About_Us_Description_No_Text"}, groups ={Priority.ONE})
	public void Update_About_Us_Description_No_Text_Continued(Method m){
		Admin admin = new Admin(driver);
		admin.navigateToSignIn();
		waitForLoad();
		admin.returnToHomepage().click();
		waitForLoad();
		Home home = new Home(driver);
		home.aboutUs().click();
		assert home.verifyAboutUsContent("") : "Updated About Us text not displayed... TC #23 failed.";
		Reporter.log("PASSED : TC #23: About Us' Description - Actual APGPCL site");
		Utils.captureScreenShot(m.getName() + " TC#23_Passed");
		quitDriver();
	}
	
	@Test(groups ={Priority.ONE})
	public void Update_About_Us_Description_Cancel(Method m) throws InterruptedException{
		Admin admin = new Admin(driver);
		admin.navigateToSignIn();
		waitForLoad();
		admin.login(getAdminUsername(), getAdminPassword());
		Thread.sleep(5000);
		ControlPanelHome cph = new ControlPanelHome(driver);
		cph.tabGeneral().click();
		cph.pageContents().click();
		Thread.sleep(2000);
		PageContent pc = new PageContent(driver);
		pc.selectPage().selectByVisibleText("About Us");
		Thread.sleep(4000);
		pc.changePageContentText("abcdef");
		pc.btnCancel().click();
		Reporter.log("PASSED : TC #24: Update 'About Us' option in Select Page");
		Utils.captureScreenShot(m.getName() + " TC#24_Passed");
		admin.isWelcomeTextDisplayed();
		quitDriver();
	}
	
	@Test(dependsOnMethods={"Update_About_Us_Description_Cancel"}, groups ={Priority.ONE})
	public void Update_About_Us_Description_Cancel_Continued(Method m){
		Admin admin = new Admin(driver);
		admin.navigateToSignIn();
		waitForLoad();
		admin.returnToHomepage().click();
		waitForLoad();
		Home home = new Home(driver);
		home.aboutUs().click();
		assert !home.verifyAboutUsContent("abcdef") : "Updated About Us text displayed... TC #25 failed.";
		Reporter.log("PASSED : TC #25: Select 'About Us' in Actual APGPCL site");
		Utils.captureScreenShot(m.getName() + " TC#25_Passed");
		quitDriver();
	}
	
	void quitDriver(){
		driver.close();
		driver.quit();
	}
	
}
