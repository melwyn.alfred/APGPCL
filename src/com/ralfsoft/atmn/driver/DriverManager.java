/**
 * 
 */
package com.ralfsoft.atmn.driver;

import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * @author Melwyn
 *
 */
public class DriverManager {

	private WebDriver driver;

	public WebDriver getDriver() {

	    System.setProperty("webdriver.ie.driver", "./Driver/ie/IEDriverServer.exe");
	    //For E.g ("webdriver.gecko.driver", "C://geckodriver.exe")
	    DesiredCapabilities dc = new DesiredCapabilities();
	    dc.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
	    WebDriver driver = new InternetExplorerDriver(dc);
		
//		driver = new FirefoxDriver();
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}
	
}
