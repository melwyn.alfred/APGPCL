/**
 * 
 */
package com.ralfsoft.atmn.platform.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.ralfsoft.atmn.driver.UISetup;

/**
 * @author Melwyn
 *
 */
public class ControlPanelHome extends UISetup {

	WebDriver driver;

	public ControlPanelHome(WebDriver webdriver){
		setDriver(webdriver);
	}
	
	public WebElement tabGeneral(){
		return getDriver().findElement(By.id("AccordionPane1_header_lblACH1"));
	}
	
	public WebElement tabDirector(){
		return getDriver().findElement(By.id("AccordionPane2_header_lblACH2"));
	}
	
	public WebElement tabPartner(){
		return getDriver().findElement(By.id("AccordionPane3_header_lblACH3"));
	}
	
	public WebElement tabSettings(){
		return getDriver().findElement(By.id("AccordionPane4_header_lblACH4"));
	}
	
	public WebElement pageContents(){
		return getDriver().findElement(By.cssSelector("a[href='PageContent.aspx']"));
	}
	
	public WebElement newsAndEvents(){
		return getDriver().findElement(By.cssSelector("a[href='NewsAndEventList.aspx']"));
	}
	
	public WebElement boardOfDirectors(){
		return getDriver().findElement(By.cssSelector("a[href='BoardOfDirectorsList.aspx']"));
	}
	
	public WebElement uploadPhotos(){
		return getDriver().findElement(By.cssSelector("a[href='UploadPhotosToFolderList.aspx']"));
	}
	
	public boolean isGeneralSubMenuDisplayed(){
		return ( pageContents().isDisplayed()  &&
				newsAndEvents().isDisplayed()  &&
				boardOfDirectors().isDisplayed()  &&
				uploadPhotos().isDisplayed() );
	}
		
	public boolean isMainMenuDisplayed(){
		return ( tabGeneral().isDisplayed()  &&
				tabDirector().isDisplayed()  &&
				tabPartner().isDisplayed()  &&
				tabSettings().isDisplayed() );
	}
	
	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

}
