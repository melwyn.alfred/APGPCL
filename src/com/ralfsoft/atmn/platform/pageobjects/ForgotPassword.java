/**
 * 
 */
package com.ralfsoft.atmn.platform.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.ralfsoft.atmn.driver.UISetup;

/**
 * @author Melwyn
 *
 */
public class ForgotPassword  extends UISetup {

	WebDriver driver;

	public ForgotPassword(WebDriver webdriver){
		setDriver(webdriver);
	}
	
	public WebElement username(){
		return getDriver().findElement(By.id("ContentPlaceHolder2_txtUsername"));
	}
	
	public WebElement email(){
		return getDriver().findElement(By.id("ContentPlaceHolder2_txtEmail"));
	}
	
	public WebElement btnSubmit(){
		return getDriver().findElement(By.id("ContentPlaceHolder2_btnSubmit"));
	}
	
	public WebElement btnCancel(){
		return getDriver().findElement(By.id("ContentPlaceHolder2_btnCancel"));
	}
	
	public boolean isPasswordResetDisplayed(){
		return (username().isDisplayed()  &&
				email().isDisplayed()  &&
				btnSubmit().isDisplayed()  &&
				btnCancel().isDisplayed());
	}
	
	public void resetPassword(String username, String email){
		if(isPasswordResetDisplayed()){
			enterDetails(username, email);
			btnSubmit().click();
		}
	}
	
	public void enterDetails(String username, String email){
		username().clear();
		username().sendKeys(username);
		email().clear();
		email().sendKeys(email);
	}
	
	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}
}
