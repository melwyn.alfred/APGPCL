/**
 * 
 */
package com.ralfsoft.atmn.tests;

/**
 * @author Melwyn
 *
 */
public class Priority {

	public static final String ONE = "priority1";
	public static final String TWO = "priority2";
	public static final String THREE = "priority3";
	public static final String FOUR = "priority4";
	public static final String FIVE = "priority5";
}
