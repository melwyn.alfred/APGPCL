/**
 * 
 */
package com.ralfsoft.atmn.driver;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;

import com.ralfsoft.atmn.core.Setup;

/**
 * @author Melwyn
 *
 */
public class UISetup extends Setup {

	protected WebDriver driver; 
	
	@BeforeMethod(alwaysRun = true)
	public void initDriver(){
		driver=new DriverManager().getDriver();
	}
	
	protected WebDriver getDriver(){
		return driver;
	}
	
	 protected void waitForLoad() {
	    new WebDriverWait(driver, 30).until((ExpectedCondition<Boolean>) wd ->
	            ((JavascriptExecutor) wd).executeScript("return document.readyState").equals("complete"));
	}
}
