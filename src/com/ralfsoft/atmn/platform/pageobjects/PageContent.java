/**
 * 
 */
package com.ralfsoft.atmn.platform.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.ralfsoft.atmn.driver.UISetup;

/**
 * @author Melwyn
 *
 */
public class PageContent extends UISetup {

	WebDriver driver;

	public PageContent(WebDriver webdriver){
		setDriver(webdriver);
	}
	
	public Select selectPage(){
		return new Select(selectPageDropDown());
	}
	
	public WebElement selectPageDropDown(){
		return getDriver().findElement(By.id("ContentPlaceHolder1_drpPage"));
	}
	
	public WebElement descriptionFrame(){
		return getDriver().findElement(By.id("ContentPlaceHolder1_edtrMessage_ctl02_ctl00"));
	}
	
	public WebElement btnSave(){
		return getDriver().findElement(By.id("ContentPlaceHolder1_btnSave"));
	}
	
	public WebElement btnCancel(){
		return getDriver().findElement(By.id("ContentPlaceHolder1_btnCancel"));
	}
	
//	public WebElement txtDescription(){
//		return getDriver().findElement(By.id("ContentPlaceHolder1_edtrMessage_ctl02_ctl01"));
//	}
	
	public WebElement txtDescription(){
		return getDriver().findElement(By.xpath("/html/body"));
	}
		
	public WebElement btnBold(){
		return getDriver().findElement(By.id("ContentPlaceHolder1_edtrMessage_ctl01_ctl03"));
	}
	
	public WebElement editedText(){
		return getDriver().findElement(By.cssSelector("span[style='font-weight: bold;']"));
	}
	
	public boolean verifyPageContentText(String text){
		getDriver().switchTo().frame(descriptionFrame());
		return getDriver().getPageSource().contains(text);
	}
		
	public boolean verifyPageContentFormatting() throws InterruptedException{
		btnBold().click();
		Thread.sleep(5000);
		descriptionFrame().sendKeys(Keys.chord(Keys.SHIFT,Keys.CONTROL,Keys.END,Keys.CONTROL,Keys.DELETE));
		descriptionFrame().sendKeys("sample");
		getDriver().switchTo().frame(descriptionFrame());
		if(getDriver().getPageSource().contains("<span style=\"font-weight: bold;\">sample</span>"))
			return true;
		else
			return false;
	}
	
	public void changePageContentText(String text) throws InterruptedException{
		Thread.sleep(5000);
		descriptionFrame().sendKeys(Keys.chord(Keys.SHIFT,Keys.CONTROL,Keys.END,Keys.CONTROL,Keys.DELETE));
		descriptionFrame().sendKeys(text);
	}
	
	public boolean isPageContentFieldsDisplayed(){
		return ( selectPageDropDown().isDisplayed()  &&
				descriptionFrame().isDisplayed()  &&
				btnSave().isDisplayed()  &&
				btnCancel().isDisplayed() );
	}
	
	public boolean verifySelectPageList(){
		try{
			selectPage().selectByVisibleText("Home");
			selectPage().selectByVisibleText("About Us");
			selectPage().selectByVisibleText("Contact Us");
			selectPage().selectByVisibleText("Opportunities");
			return true;
		}
		catch(NoSuchElementException e){
			return false;
		}
	}
	
	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

}
