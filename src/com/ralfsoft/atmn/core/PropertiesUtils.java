/**
 * 
 */
package com.ralfsoft.atmn.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Melwyn
 *
 */
public class PropertiesUtils {

	Properties prop = new Properties();
	InputStream input = null;
	
	public Properties getProperties(String path){
	try {
		File f = new File(path);
		input = new FileInputStream(f.getCanonicalPath()); 
		
		// load a properties file
		prop.load(input);
		return prop;
	} catch (IOException ex) {
		ex.printStackTrace();
	} finally {
		if (input != null) {
			try {
				input.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	return prop;
	
  }
}
