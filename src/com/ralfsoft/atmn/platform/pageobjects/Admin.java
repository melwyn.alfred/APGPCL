/**
 * 
 */
package com.ralfsoft.atmn.platform.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.ralfsoft.atmn.driver.UISetup;

/**
 * @author Melwyn
 *
 */
public class Admin extends UISetup {

	WebDriver driver;

	public Admin(WebDriver webdriver){
		setDriver(webdriver);
	}
	
	public void navigateToSignIn(){
		driver.get(getAdminURL());
	}

	public WebElement userName(){
		return getDriver().findElement(By.id("ContentPlaceHolder2_txtUserName"));
	}

	public WebElement password(){
		return getDriver().findElement(By.id("ContentPlaceHolder2_txtPassword"));
	}

	public WebElement btnSignIn(){
		return getDriver().findElement(By.id("ContentPlaceHolder2_btnSignIn"));
	}

	public void login(String username, String password){
		userName().sendKeys(username);
		password().sendKeys(password);
		btnSignIn().click();
	}

	public WebElement returnToHomepage(){
		return getDriver().findElement(By.cssSelector("a[href='../Default.aspx']"));
	}


	public WebElement welcomeText(){
		return getDriver().findElement(By.cssSelector("#ContentPlaceHolder1_UpdatePanel2 > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(1) > h1:nth-child(1)"));
	}

	public WebElement forgotPassword(){
		return getDriver().findElement(By.cssSelector("a[href='ForgetPassword.aspx']"));
	}

	public boolean isWelcomeTextDisplayed(){
		try{
				if(welcomeText().isDisplayed()){
					if(welcomeText().getText().contains("WELCOME TO APGPCL CONTROL PANEL"))
					return true;
					else
					return false;
				}
				return false;
		}
		catch(Exception e){
			return false;
		}
	}
	
	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

}
